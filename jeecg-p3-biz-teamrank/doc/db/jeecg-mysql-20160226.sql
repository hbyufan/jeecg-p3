
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_s_team_person
-- ----------------------------
DROP TABLE IF EXISTS `jp_team_person`;
CREATE TABLE `jp_team_person` (
  `id` varchar(36) NOT NULL,
  `create_name` varchar(50) DEFAULT NULL COMMENT '创建人名称',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人登录名称',
  `create_date` datetime DEFAULT NULL COMMENT '创建日期',
  `update_name` varchar(50) DEFAULT NULL COMMENT '更新人名称',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人登录名称',
  `update_date` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(50) DEFAULT NULL COMMENT '所属部门',
  `sys_company_code` varchar(50) DEFAULT NULL COMMENT '所属公司',
  `name` varchar(32) DEFAULT NULL COMMENT '名称',
  `img_src` varchar(50) DEFAULT NULL COMMENT '头像路径',
  `introduction` longtext COMMENT '简介',
  `jion_date` datetime DEFAULT NULL COMMENT '加入时间',
  `is_join` int(11) DEFAULT NULL COMMENT '是否参与；1为是，0为否',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
